﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Entities
{
    public class ATMContext: DbContext
    {
        public ATMContext(): base("name = ATM")
        {
                
        }
        public DbSet<User> Users { get; set; }
    }
}
