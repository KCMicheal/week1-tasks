﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM
{
    public class User
    {
        public int Id { get; set; }
        [MaxLength(50)]
        [MinLength(4)]
        public string Name { get; set; }
        public string Pin { get; set; }
        public int AccountNumber { get; set; }
        public int AccountBalance { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Created { get; set; }
    }
}